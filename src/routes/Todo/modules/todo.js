// ------------------------------------
// Constants
// ------------------------------------
export const TODO_ADD = 'TODO_ADD'
export const TODO_COMPLETE = 'TODO_COMPLETE'

// ------------------------------------
// Actions
// ------------------------------------
let nextTaskId = 1;
export function add(value = { text: "", done: false }) {
  value.id = nextTaskId++;
  return {
    type: TODO_ADD,
    payload: value
  }
}

export function complete(id, comp = true) {
  return {
    type: TODO_COMPLETE,
    id: id,
    done: comp,
  }
}

/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export const actions = {
  add,
  complete,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [TODO_ADD]: (state, action) => {
    return state.concat(action.payload)
  },
  [TODO_COMPLETE]: (state, action) => {
    const newState = state.concat();
    newState.filter(s => s.id === action.id).map(s => s.done = action.done);
    return newState;
  },
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = [];
export default function todoReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
