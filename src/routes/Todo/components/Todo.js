import React from 'react'
import PropTypes from 'prop-types'

export class Todo extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.newTaskText = "";

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleAddTask = this.handleAddTask.bind(this);
  }

  handleTextChange(event) {
    console.log(event);
    this.newTaskText = event.target.value;
  }

  handleAddTask() {
    if (this.newTaskText) {
      this.props.add({ text: this.newTaskText });
    } else {
      alert("Input your todo text.");
    }
  }

  render() {
    console.log(this.props.todo)
    var list = [];
    for (let task of this.props.todo) {
      let comp = ()=> this.props.complete(task.id);
      list.push(<li key={task.id}>
        {task.text}{task.done ? "✓" : <button className='btn' onClick={comp}>完了にする</button>}
      </li>)
    }
    return <div style={{ margin: '0 auto' }} >
      <h2>TODO</h2>
      {this.props.todo.length}
      <ul>
        {list}
      </ul>
      <hr></hr>
      <input type='text' onChange={this.handleTextChange}></input>
      <button className='btn btn-primary' onClick={this.handleAddTask}>
        Add
      </button>
    </div>
  }
}

Todo.propTypes = {
  todo: PropTypes.array.isRequired,
  add: PropTypes.func.isRequired,
}

export default Todo
